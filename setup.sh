#!/bin/bash
#
# https://wiki.archlinux.org/title/Installation_guide

setkeymap() {
    clear
    #echo "loadkeys de-latin1"
    #loadkeys de-latin1
    echo "timedatectl set-ntp true"
    timedatectl set-ntp true
    pressanykey
}

diskpart() {
    clear
    lsblk -f
    read -p "Select disk (e.g. /dev/sda): " disk
    echo "Clear all partition data"
    sgdisk -Z "${disk}"
    echo "Create EFI boot partition"
    sgdisk -n 1:0:+300M -t 1:ef00 -c 1:"GRUB" "${disk}"
    echo "Create root partition"
    sgdisk -n 2:0:0     -t 2:8300 -c 2:"ROOT" "${disk}"
    sgdisk -p "${disk}"
    echo "partprobe ${disk}"
    partprobe "${disk}"
    pressanykey
}

formatdevices() {
    clear
    echo "Formatting partition ${disk}1 as fat32"
    echo "mkfs.vfat -F32 -n \"GRUB\" ${disk}1"
    mkfs.vfat -F32 -n "GRUB" "${disk}1"
    echo "Formatting partition ${disk}2 as btrfs"
    echo "mkfs.btrfs -L \"ROOT\" -f ${disk}2"
    mkfs.btrfs -L "ROOT" -f "${disk}2"

    echo "mount ${disk}2 /mnt"
    mount "${disk}2" /mnt
    echo "btrfs subvolume create /mnt/@"
    btrfs subvolume create /mnt/@
    echo "btrfs subvolume create /mnt/@home"
    btrfs subvolume create /mnt/@home
    echo "btrfs subvolume create /mnt/@var_cache"
    btrfs subvolume create /mnt/@var_cache
    echo "btrfs subvolume create /mnt/@var_log"
    btrfs subvolume create /mnt/@var_log
    echo "btrfs subvolume create /mnt/@swap"
    btrfs subvolume create /mnt/@swap
    echo "umount /mnt"
    umount /mnt
    pressanykey
}

mountparts() {
    opts="defaults,noatime,space_cache,ssd,compress=zstd,commit=120"

    clear
    echo "mount -o subvol=@,${opts} ${disk}2 /mnt"
    mount -o subvol=@,"${opts}" "${disk}2" /mnt
    echo "mkdir -p /mnt/{boot/efi,home,var/{cache,log},swap}"
    mkdir -p /mnt/{boot/efi,home,var/{cache,log},swap}
    echo "mount -o subvol=@home,${opts} ${disk}2 /mnt/home"
    mount -o subvol=@home,"${opts}" "${disk}2" /mnt/home
    echo "mount -o subvol=@var_cache,${opts} ${disk}2 /mnt/var/cache"
    mount -o subvol=@var_cache,"${opts}" "${disk}2" /mnt/var/cache
    echo "mount -o subvol=@var_log,${opts} ${disk}2 /mnt/var/log"
    mount -o subvol=@var_log,"${opts}" "${disk}2" /mnt/var/log
    echo "mount -o subvol=@swap,${opts} ${disk}2 /mnt/swap"
    mount -o subvol=@swap,"${opts}" "${disk}2" /mnt/swap
    echo "mount ${disk}1 /mnt/boot/efi"
    mount "${disk}1" /mnt/boot/efi
    pressanykey
}

# Create a swap file
# https://wiki.archlinux.org/title/Btrfs#Swap_file
formatswapdevice() {
    echo "truncate -s 0 /mnt/swap/swapfile"
    truncate -s 0 /mnt/swap/swapfile
    echo "chattr +C /mnt/swap/swapfile"
    chattr +C /mnt/swap/swapfile
    echo "btrfs property set /mnt/swap/swapfile compression none"
    btrfs property set /mnt/swap/swapfile compression none
    swapsize="$(cat /proc/meminfo | grep MemTotal | awk '{ print $2 }')"
    swapsize="$((${swapsize}/1000))"
    echo "dd if=/dev/zero of=/mnt/swap/swapfile bs=1M count=${swapsize} status=progress"
    dd if=/dev/zero of=/mnt/swap/swapfile bs=1M count="${swapsize}" status=progress
    echo "chmod 600 /mnt/swap/swapfile"
    chmod 600 /mnt/swap/swapfile
    echo "mkswap /mnt/swap/swapfile"
    mkswap /mnt/swap/swapfile
    echo "swapon /mnt/swap/swapfile"
    swapon /mnt/swap/swapfile
    pressanykey
}

installbase() {
    pkgs="base base-devel linux linux-firmware nano vim amd-ucode btrfs-progs"

    clear
    echo "pacstrap /mnt ${pkgs}"
    pacstrap /mnt ${pkgs}
    pressanykey
}

archgenfstab() {
    clear
    echo "genfstab -L -p /mnt > /mnt/etc/fstab"
    genfstab -L -p /mnt > /mnt/etc/fstab
    cat /mnt/etc/fstab
    pressanykey
}

archchroot() {
    echo "arch-chroot /mnt"
    cp "${0}" /mnt/root
    chmod 755 "/mnt/root/$(basename ${0})"
    arch-chroot /mnt "/root/$(basename ${0})" --chroot "${1}" "${2}"
    rm "/mnt/root/$(basename ${0})"
    echo "exit"
}

archsettime() {
    clear
    echo "Setting the system time and locale to German"
    echo "ln -sf /usr/share/zoneinfo/Australia/Melbourne /mnt/etc/localtime"
    ln -sf /usr/share/zoneinfo/Australia/Melbounr /mnt/etc/localtime
    archchroot settimeutc
    pressanykey
}

archsettimeutcchroot() {
    echo "hwclock --systohc --utc"
    hwclock --systohc --utc
    exit
}

archsetlocale() {
    clear
    echo "echo \"LANG=en_US.UTF-8\" > /mnt/etc/locale.conf"
    echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf
    echo "echo \"LC_COLLATE=C\" >> /mnt/etc/locale.conf"
    echo "LC_COLLATE=C" >> /mnt/etc/locale.conf
    echo "echo \"LC_TIME=en_US.UTF-8\" >> /mnt/etc/locale.conf"
    echo "LC_TIME=en_US.UTF-8" >> /mnt/etc/locale.conf
    echo "sed -i '/#en_US.UTF-8/s/^#//g' /mnt/etc/locale.gen"
    sed -i '/#en_US.UTF-8/s/^#//g' /mnt/etc/locale.gen
    echo "sed -i '/#en_US.UTF-8/s/^#//g' /mnt/etc/locale.gen"
    sed -i '/#en_US.UTF-8/s/^#//g' /mnt/etc/locale.gen
    archchroot setlocale
    pressanykey
}

archsetlocalechroot() {
    echo "locale-gen"
    locale-gen
    echo "localectl --no-convert set-x11-keymap us"
    localectl --no-convert set-x11-keymap us
    exit
}

archsetkeymap() {
    clear
    echo "echo \"KEYMAP=us\" > /mnt/etc/vconsole.conf"
    echo "KEYMAP=us" > /mnt/etc/vconsole.conf
    pressanykey
}

archsethostname() {
    clear
    read -p "Please enter hostname: " hostname
    echo "echo \"${hostname}\" > /mnt/etc/hostname"
    echo "${hostname}" > /mnt/etc/hostname
    echo "127.0.0.1 localhost" >> /mnt/etc/hosts
    echo "::1       localhost" >> /mnt/etc/hosts
    echo "127.0.1.1 ${hostname}.localdomain ${hostname}" >> /mnt/etc/hosts
    cat /mnt/etc/hosts
    pressanykey
}

archinitramfs() {
    clear
    "${EDITOR}" /mnt/etc/mkinitcpio.conf
    archchroot initramfs
    pressanykey
}

archinitramfschroot() {
    echo "mkinitcpio -P"
    mkinitcpio -P
    exit
}

archsetrootpassword() {
    clear
    archchroot setrootpassword
    pressanykey
}

archsetrootpasswordchroot() {
    echo "passwd root"
    passed=1
    while [[ "${passed}" != "0" ]]; do
        passwd root
        passed="$?"
    done
    exit
}

archgrubinstall() {
    clear
    echo "Installing GRUB boot loader"
    echo "pacstrap /mnt grub grub-btrfs efibootmgr os-prober ntfs-3g"
    pacstrap /mnt grub grub-btrfs efibootmgr os-prober ntfs-3g
    pressanykey

    clear
    archchroot grubinstall
    pressanykey
}

archgrubinstallchroot() {
    echo "grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB"
    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
    exit
}

archgrubconfig() {
    clear
    echo "sed -i 's/\(GRUB_CMDLINE_LINUX_DEFAULT=\"[^\"]*\)\"/\1 amdgpu.dc=0\"/' /mnt/etc/default/grub"
    sed -i 's/\(GRUB_CMDLINE_LINUX_DEFAULT="[^"]*\)"/\1 amdgpu.dc=0"/' /mnt/etc/default/grub
    echo "echo \"GRUB_DISABLE_OS_PROBER=false\" >> /mnt/etc/default/grub"
    echo "GRUB_DISABLE_OS_PROBER=false" >> /mnt/etc/default/grub
    "${EDITOR}" /mnt/etc/default/grub
    pressanykey

    clear
    archchroot grubconfig
    pressanykey
}

archgrubconfigchroot(){
    echo "mkdir -p /boot/grub"
    mkdir -p /boot/grub
    echo "grub-mkconfig -o /boot/grub/grub.cfg"
    grub-mkconfig -o /boot/grub/grub.cfg
    exit
}

archadduser() {
    clear
    archchroot adduser
    echo "sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /mnt/etc/sudoers"
    sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /mnt/etc/sudoers
    pressanykey
}

archadduserchroot() {
    read -p "Please enter username: " username
    echo "useradd -mG wheel ${username}"
    useradd -mG wheel "${username}"
    echo "passwd ${username}"
    passed=1
    while [[ "${passed}" != "0" ]]; do
        passwd "${username}"
        passed="$?"
    done
    exit
}

archenablenetworkmanager() {
    clear
    echo "pacstrap /mnt networkmanager reflector rsync"
    pacstrap /mnt networkmanager reflector rsync
    archchroot enablenetworkmanager
    pressanykey
}

archenablenetworkmanagerchroot() {
    echo "systemctl enable NetworkManager"
    systemctl enable NetworkManager
    exit
}

unmountdevices() {
    clear
    echo "swapoff /mnt/swap/swapfile"
    swapoff /mnt/swap/swapfile
    echo "umount -R /mnt"
    umount -R /mnt
    pressanykey
}

pressanykey() {
    read -n1 -p "Press any key to continue."
}

while (( "$#" )); do
    case "${1}" in
        --chroot) chroot=1
                  command="${2}"
                  args="${3}" ;;
    esac
    shift
done

if [[ "${chroot}" == "1" ]]; then
    case "${command}" in
        'settimeutc') archsettimeutcchroot ;;
        'setlocale') archsetlocalechroot ;;
        'initramfs') archinitramfschroot ;;
        'setrootpassword') archsetrootpasswordchroot ;;
        'grubinstall') archgrubinstallchroot ;;
        'grubconfig') archgrubconfigchroot ;;
        'adduser') archadduserchroot ;;
        'enablenetworkmanager') archenablenetworkmanagerchroot ;;
    esac
else
    setkeymap
    diskpart
    formatdevices
    mountparts
    formatswapdevice
    installbase
    archgenfstab
    archsettime
    archsetlocale
    archsetkeymap
    archsethostname
    archinitramfs
    archsetrootpassword
    archgrubinstall
    archgrubconfig
    archadduser
    archenablenetworkmanager
    unmountdevices

    clear
    echo "Done! Restart the machine by typing reboot."
fi

exit 0
